$(document).ready(function() {

	var owlTop = $('.owl-carousel'),
		owlTBot = $('.punkt_carousel'),
		footerMenu = $('.footer-menu'),
		punkts = $('.punkt_carousel div.item');
		toogle_menu = $('.toogle_menu');

	owlTop.owlCarousel({
	    items:1,
	});

	// Listen to owl events:
	owlTop.on('changed.owl.carousel', function(event) {
		punkts.removeClass('active');
		punkts.eq(event.item.index).addClass('active');
	    // owlTBot.trigger('to.owl.carousel', [event.item.index]);
	})
	punkts.click(function(event) {
		owlTop.trigger('to.owl.carousel', punkts.index(this));
	});

	toogle_menu.click(function() {
		footerMenu.slideToggle(500);
		$(this).toggleClass('closed');
	});

	if ($(window).width() < 960) {
		toogle_menu.addClass('closed');
		footerMenu.hide();
	};

	$('.etaps > div').click(function(){
		if ($(this).hasClass('active')) {
			$(this).find('.pomosh').css('display','none');
			$(this).removeClass('active');
		}
		else{
			$('.pomosh').css('display','none');
			$('.etaps > div').removeClass('active');
			$(this).find('.pomosh').css('display','block');
			$(this).addClass('active');
		}
		
	})
});